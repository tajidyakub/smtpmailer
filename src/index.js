const debug = require('debug')('smtp')
const nodemailer = require('nodemailer')
const htmlToText = require('nodemailer-html-to-text').htmlToText

debug(`Booting up Smtpmailer module..`)

function createTransport (smtp) {
  let isSecure = false
  if (smtp.port === 465) isSecure = true
  debug(`Creating Transport with ${smtp}`)
  return nodemailer.createTransport({
    host: smtp.host,
    port: smtp.port,
    secure: isSecure,
    auth: {
      user: smtp.auth.user,
      pass: smtp.auth.pass
    }
  })
}

function send (message, transport) {
  debug(`Send the message ${message}`)
  return transport.sendMail(message,
    (info) => {
      debug(`Receiving result ${info}`)
      if (info.err) {
        debug(`Sending failed ${info.err.message}`)
        return {
          status: 'error',
          message: info.err.message
        }
      }
      debug(`Sending success`)
      const result = { status: 'success',
        message: 'Email message sent successfully' }

      if (info.hasOwnProperty('messageId')) {
        Object.assign(result, {
            messageId: info.messageId})}

      if (nodemailer.getTestMessageUrl(info)) {
        Object.assign(result, {
            messagePreviewUrl: nodemailer.getTestMessageUrl(info)})}
      debug(`Returning result ${result}`)
      return result })}

async function smtpmailer (options) {
  if (!options.hasOwnProperty('smtp')
    || !options.hasOwnProperty('message')) {
      debug(`Options incomplete or misconfigured ${options}`)
      Promise.reject(`Options definitions incomplete`)}

   return createTransport(options.smtp)
    .then(async transport => {
      await transport.use('compile', htmlToText({}))
      return send (message, transport)})}

module.exports = smtpmailer
