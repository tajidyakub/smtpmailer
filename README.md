# Simple SMTP Mailer Module

> Simple module to send email messages through defined smtp host from your application.

Tajid Yakub <tajid.yakub@gmail.com>

## Install

Install the module using any node package manager

```
$ npm install --save https://bitbucket.org/tajidyakub/smtpmailer.git
```

## Usage

Use it in your application with supplying `options` object consist of `smtp` configuration and `message` object contains the email you wish to send. Example using `ethereal` test account.


``` javascript
const options = {
    smtp: {
        host: 'smtp.ethereal.com',
        port: 587
        auth: {
            user: 'user@ethereal.com',
            pass: 'password'
        }
    },
    message: {
        from: 'sender@email.com',
        to: 'recipient@email.com',
        subject: 'Testing Email Address',
        html: `<p>Testing the email</p>`
    }
}
```